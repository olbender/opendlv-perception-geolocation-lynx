/*
 * Copyright (C) 2021 Ola Benderius
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <iostream>
#include <string>

#include <Eigen/Eigen>

#include "cluon-complete.hpp"
#include "opendlv-standard-message-set.hpp"
#include "WGS84toCartesian.hpp"

int32_t main(int32_t argc, char **argv)
{
  int32_t retCode{0};
  auto commandlineArguments = cluon::getCommandlineArguments(argc, argv);
  if ( (0 == commandlineArguments.count("cid")) ||
       (0 == commandlineArguments.count("freq"))) {
    std::cerr << argv[0] << " is a Kalman filter for the Lynx racing car."
      << std::endl
      << "Usage:   " << argv[0] << " --cid=<CID> --freq=<Frequency> "
      << "[--verbose]"
      << std::endl;
    retCode = 1;
  } else {
    uint16_t const cid = std::stoi(commandlineArguments["cid"]);
    bool const verbose{commandlineArguments.count("verbose") != 0};
    float const freq = std::stof(commandlineArguments["freq"]);

    cluon::OD4Session od4(cid);

    // Define all the Eigen matrices here
    Eigen::Matrix<double, 2, 1> state;


    // Initialize matrices
    state = Eigen::Matrix<double, 2, 1>::Zero();




    // Data trigger functions are incleded below
    auto onGeodeticWgs84Reading{[&verbose](cluon::data::Envelope &&envelope)
      {
        if (envelope.senderStamp() == 0) {
          auto msg = 
            cluon::extractMessage<opendlv::proxy::GeodeticWgs84Reading>(
                std::move(envelope));
          double latitude = msg.latitude();
          double longitude = msg.longitude();
          if (verbose) {
            std::cout << "Got WGS84 coordinate, lat=" << latitude << ", lon="
              << longitude << std::endl;
          }
        }
      }};
    
    auto onAccelerationReading{[&verbose](cluon::data::Envelope &&envelope)
      {
        if (envelope.senderStamp() == 0) {
          auto msg = 
            cluon::extractMessage<opendlv::proxy::AccelerationReading>(
                std::move(envelope));
          float ax = msg.accelerationX();
          float ay = msg.accelerationY();
          if (verbose) {
            std::cout << "Got acceleration, ax=" << ax << ", ay="
              << ay << std::endl;
          }
        }
      }};
    


    // The time trigger, main function, is defined below
    auto atFrequency{[&state, &od4, &verbose]() -> bool
      {
        float vx{0.0f};
        float vy{0.0f};
        float yawRate{0.0f};

        // Run the filter here, in the end updating your state
        // .. To get started with Eigen: https://eigen.tuxfamily.org/dox/GettingStarted.html





        // Transform double to float, and then send as output from the
        // microservice
        vy = static_cast<float>(state[0]);
        yawRate = static_cast<float>(state[1]);

        opendlv::logic::sensation::Equilibrioception equilibrioception;
        equilibrioception.vx(vx);
        equilibrioception.vy(vy);
        equilibrioception.yawRate(yawRate);
        
        cluon::data::TimeStamp sampleTime;
        od4.send(equilibrioception, sampleTime, 0);

        return true;
      }};

    // Register the three data triggers, each checked in a separate thread
    od4.dataTrigger(opendlv::proxy::GeodeticWgs84Reading::ID(),
        onGeodeticWgs84Reading);
    od4.dataTrigger(opendlv::proxy::AccelerationReading::ID(),
        onAccelerationReading);

    // Register the time trigger, spawning a thread that blocks execution 
    // until CTRL-C is pressed
    od4.timeTrigger(freq, atFrequency);
  }
  return retCode;
}

